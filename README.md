# Aevol User Interface

## Set up your development environment

### Install the django project and depencies
You need to install Docker on your machine. You can follow this tutorial : https://docs.docker.com/get-docker/.

By default docker can only be ran by the root user, or by prefacing every docker command by "sudo". If you would like to run docker commands without sudo, there is a way described here : https://docs.docker.com/engine/install/linux-postinstall/.

Now you can build and start the containers with the following commands (prefaced by `sudo` if you did not configure docker to run as non-root user):

```sh
docker compose build
docker compose up
```

If you want to check that your newly created containers are running, run the following command:

```sh
docker ps
```

You should get an output similar to this one:

```
CONTAINER ID   IMAGE                COMMAND                  CREATED         STATUS         PORTS                    NAMES
f0068ef119f6   aevol_ui_api_dev     "python manage.py ru…"   2 minutes ago   Up 2 minutes   0.0.0.0:6400->6400/tcp   aevol_ui_api_dev
72b018bfb089   aevol_ui_front_dev   "docker-entrypoint.s…"   4 days ago      Up 2 minutes   0.0.0.0:80->5173/tcp     aevol_ui_front_dev
```

If you want to stop the app, press ctrl-c from within the terminal it's running in.

And restart with:

```sh
docker compose up
```

Please note that you usually don't need to restart your app in order for your modifications to take effect. An auto-reload is at work in dev mode ;)

## Usage
Both the back and the front run automatically after the containers start. They do however expect to find some data to be visualized. Your data/ directory should contain the following files (all of which being issued by aevol_run):


```sh
last_gener.txt       # the latest available generation
generation_step.txt  # the step between 2 available generations
environment.json     # the environment file
indiv_xxxxxxxxx.json # data about the individual of interest at generation xxx
grid_xxxxxxxxx.json  # the grid at generation xxx
```

By default, the front listens on port 80 and the back on port 6400. You can check that the api is responding with your data by visiting one or more of the following endpoints (xxx being a generation number):
* http://localhost:6400/api/latest-gener
* http://localhost:6400/api/generation-step
* http://localhost:6400/api/xxx/rnas
* http://localhost:6400/api/10/proteins
* http://localhost:6400/api/10/fitness
* http://localhost:6400/api/10/grid

You're all set, just visit http://localhost and have fun!
