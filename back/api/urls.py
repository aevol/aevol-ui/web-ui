from django.urls import path

from . import views

urlpatterns = [
    path("latest-gener", views.latest_gener, name="latest-gener"),
    path("generation-step", views.generation_step, name="generation-step"),
    path(
        "<int:generation>/phenotype",
        views.get_svg,
        {"img_type": "phenotype"},
        name="phenotype-svg",
    ),
    path(
        "<int:generation>/proteome",
        views.get_svg,
        {"img_type": "proteome"},
        name="proteome-svg",
    ),
    path("<int:generation>/grid", views.get_svg, {"img_type": "grid"}, name="grid-svg"),
    path(
        "<int:generation>/genes",
        views.get_svg,
        {"img_type": "genes"},
        name="genes-svg",
    ),
    path("<int:generation>/rnas", views.get_svg, {"img_type": "rnas"}, name="rnas-svg"),
]
