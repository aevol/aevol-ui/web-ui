import os

from django.conf import settings

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .utils import search_image_or_generate


@api_view(["GET"])
def latest_gener(request):
    if request.method == "GET":
        try:
            with open(
                os.path.join(settings.BASE_DIR, "data", "last_gener.txt"),
                "r",
                encoding="utf-8",
            ) as stream:
                lines = stream.readline().splitlines()
                return Response(lines[0])
        except OSError:
            return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(["GET"])
def generation_step(request):
    if request.method == "GET":
        try:
            with open(
                os.path.join(settings.BASE_DIR, "data", "generation_step.txt"),
                "r",
                encoding="utf-8",
            ) as stream:
                lines = stream.readline().splitlines()
                return Response(lines[0])
        except OSError:
            return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(["GET"])
def get_svg(request, generation, img_type=None):
    if img_type is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    if request.method == "GET":
        return search_image_or_generate(generation, img_type)
