import os
from pathlib import Path
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response

from aevol.visual.draw import draw_all

# ------------------------------------------------------- Global variables
img_dir = Path("data/images/")  # Path to images dir
data_dir = Path("data/")  # Path to data dir

if not img_dir.exists():
    img_dir.mkdir(parents=True)


def format_filename(prefix, generation, extension):
    return prefix + "_" + str(generation).zfill(9) + extension


# Send the image (generate if it has not been computed yet)
def search_image_or_generate(generation, prefix):
    # Check if the image was already computed...
    response = search_image(generation, prefix)

    # If not generate output for the current generation and retry
    if response.status_code == 404:
        create_generation_output(generation)
        response = search_image(generation, prefix)
    return response


# Send the image if it has already been computed
def search_image(generation, prefix):
    try:
        with open(
            os.path.join(
                settings.BASE_DIR, img_dir, format_filename(prefix, generation, ".svg")
            ),
            "r",
            encoding="utf-8",
        ) as stream:
            return Response(stream.read())
    except OSError:
        return Response(status=status.HTTP_404_NOT_FOUND)


def create_generation_output(generation):
    indiv_path = os.path.join(
        settings.BASE_DIR, data_dir, format_filename("indiv", generation, ".json")
    )
    env_path = os.path.join(settings.BASE_DIR, data_dir, "environment.json")
    grid_path = os.path.join(
        settings.BASE_DIR, data_dir, format_filename("grid", generation, ".json")
    )

    indiv_file = indiv_path if os.path.isfile(indiv_path) else None
    env_file = env_path if os.path.isfile(env_path) else None
    grid_file = grid_path if os.path.isfile(grid_path) else None

    img_fnames = {
        "rnas": format_filename("rnas", generation, ".svg"),
        "genes": format_filename("genes", generation, ".svg"),
        "phenotype": format_filename("phenotype", generation, ".svg"),
        "proteome": format_filename("proteome", generation, ".svg"),
        "grid": format_filename("grid", generation, ".svg"),
    }

    draw_all(
        indiv_file, env_file, grid_file, img_dir, display_legend=True, fnames=img_fnames
    )
