<script lang="ts">
  import {onMount} from "svelte";

  import Figure from "./Figure.svelte";

  // Available figures
  let available_figures = ['RNAS', 'Genes', 'Phenotype', 'Proteome', 'Grid'];

  // Figures requested by the user. By default, all the figures available.
  let requested_figures = available_figures;

  // Array that contains the html code for each requested visualization
  let figures = new Map<string, string>();
  let cached_figures = new Map<number, Map<string, string>>();

  let generation = 0;         // The requested generation
  let last_generation = 0;    // The last generation that was computed by aevol simulation
  let generation_step = 10;   // The number of generation between each computed generation

  let play = false            // Whether we are currently "playing" in video mode
  let interval_id;            // Id of the interval used to call periodically the api
  let delay = 2000;           // Delay between each image in video mode

  let label = "";

  // Called whenever requested_figures changes (including initialization)
  $: onRequestedFiguresChange(requested_figures);

  // Runs when the component is first rendered to the DOM
  onMount(async() => {
    // Get the last generation number
    getLastGeneration()

    // Get the generation_step
    getGenerationStep()
  });

  // Call the api to get the last computed generation
  async function getLastGeneration() {
    await fetch("http://localhost:6400/api/latest-gener").then((res) => {
      res.json().then((res) => last_generation = Number(res))
    });
  }

  // Call the api to get the generation step
  async function getGenerationStep() {
      await fetch("http://localhost:6400/api/generation-step").then((res) => {
          res.json().then((res) => generation_step = Number(res))
      });
  }

  async function updateFigures() {
    figures = await figuresFromCache(generation, requested_figures);
  }

  async function onRequestedFiguresChange(requested_figures) {
    await updateFigures();
  }

  async function figuresFromCache(generation, requested_figures) {
    // Initialize cache entry if absent
    if (! cached_figures.has(generation)) {
      cached_figures.set(generation, new Map<string, string>());
    }

    // Retrieve data from cache
    let cur_gener_cache = cached_figures.get(generation);

    // Add missing figures if any
    for await(let fig_type of requested_figures) {
      if (!cur_gener_cache.has(fig_type)) {
        let fig = await fetchFigure(generation, fig_type.toLowerCase());
        cur_gener_cache.set(fig_type, fig);
      }
    }

    // Construct map with **only** the requested figures
    let cur_figs = new Map<string, string>();
    for (let fig_type of requested_figures) {
      cur_figs.set(fig_type, cur_gener_cache.get(fig_type));
    }

    return cur_figs;
  }

  async function fetchFigure(generation, type) {
    let fig = await fetch("http://localhost:6400/api/" + generation + "/" + type)
    .then((res) => res.json());

    // Strip svg of any width and height parameters
    fig = fig.replace(/(<svg.*)( (width)="[^"]*")/, "$1")
    .replace(/(<svg.*)( (height)="[^"]*")/, "$1");

    return fig;
  }

  function getClosestGeneration(requested_generation, disable_alerts = false) {
    if (requested_generation > last_generation) {
      if (! disable_alerts) {
        alert("The last computed generation is " + last_generation + ".\n" +
            "Showing last computed generation.");
      }
      return last_generation;
    }
    else if (requested_generation < 0) {
      if (! disable_alerts) {
        alert("The generation cannot be negative.\n" +
            "Showing generation 0.");
      }
      return 0;
    }
    else if (requested_generation % generation_step != 0) {
      let closest_generation = requested_generation - requested_generation % generation_step;
      if (! disable_alerts) {
        alert("Generation must be a multiple of " + generation_step + ".\n" +
            "Showing generation " + closest_generation + " instead.");
      }
      return closest_generation;
    }
    return requested_generation;
  }

  // Update figures when the user changes the value in the "generation" input,
  // e.g. by pressing "enter" or by clicking on the built-in stepUp or stepDown
  // arrows
  function onGenerationChange() {
    generation = getClosestGeneration(generation);
    updateFigures();
  }

  // Update the visualization with the previous generation
  function previousVisualization() {
    generation = getClosestGeneration(generation - generation_step);
    updateFigures();
  }

  // Update the visualization with the next generation
  function nextVisualization() {
    generation = getClosestGeneration(generation + generation_step);
    updateFigures();
  }

  // Start playing "video"
  function playVideo() {
    if (! play) {
      play = true;
      // function called every {time_laps} milliseconds
      interval_id = setInterval(() => {
        getLastGeneration();
        if (generation + generation_step <= last_generation) {
          generation = generation + generation_step;
          updateFigures();
        }
      }, delay);
    }
  }

  // Stop playing the "video"
  function stopVideo() {
    if (play) {
      clearInterval(interval_id);
      play = false;
    }
  }

  // Display the images (if cached) when we move the range input cursor
  function handleInputRange() {
    if (cached_figures.has(generation)) {
      updateFigures();
      label = "";
    } else {
      label = "this generation was not computed yet, release mouse to compute";
    }
  }

  // Display the image (regardless of cache status) when we release the range input cursor
  function handleInputRangeMouseUp() {
    updateFigures();
    label = "";
  }
</script>

<!-- checkbox : the user can choose the visualizations he wants to display -->
<div class = "visualization_choice">
  {#each available_figures as figure}
    <label class="mcui-checkbox">
      <input class="checkbox" type="checkbox" bind:group={requested_figures} value={figure} checked/>
      {figure}
    </label>
  {/each}

  <!-- generation choice: the user can choose the generation used to compute the visualizations  -->
  <label class="generation_choice">
    Generation:
    <!-- button to compute visualizations for the previous generation  -->
    <button on:click={previousVisualization}>
      <i class="fa fa-arrow-left" aria-hidden="true"></i>
    </button>
    <!-- text input : the user can enter the generation he wants -->
    <input type="number" required bind:value={generation} min="0" max={last_generation} step={generation_step}
           on:change={onGenerationChange}/>
    <!-- button to compute visualizations for the next generation  -->
    <button on:click={nextVisualization}>
      <i class="fa fa-arrow-right" aria-hidden="true"></i>
    </button>
  </label>
</div>

<!-- videos commands -->
<div class="video_options">
  <button on:mousedown={playVideo} disabled={play}>
    <i class="fa fa-play" aria-hidden="true"></i>
  </button>
  <button on:mousedown={stopVideo} disabled={!play}>
    <i class="fa fa-stop" aria-hidden="true"></i>
  </button>
  <!-- range input to scroll through the visualizations -->
  <input class="input-range" type="range" bind:value={generation} min="0" max={last_generation} step={generation_step} on:input={handleInputRange} on:mouseup={handleInputRangeMouseUp}/>
  <div class="label"> &nbsp;{label}&nbsp; </div>
</div>

<!-- section to display the requested visualization -->
<div class=figures>
  {#each figures.values() as figure}
    <Figure src={figure}></Figure>
  {/each}
</div>


<style>
  .figures{
    display:flex;
    flex-wrap: wrap;
  }

  .visualization_choice {
    display: flex;
    flex-wrap:wrap;
    font-size: 1em;
    font-weight: 600;
    color: #13488ddf;
    align-items: center;
    justify-content: space-around;
    margin-top: 1%;
  }

  .checkbox:checked {
    outline: 1px solid grey;
    accent-color: white;
  }

  .checkbox {
    width: 1.6em;
    height: 1.6em;
  }

  .mcui-checkbox{
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .generation_choice {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .generation_choice input{
    margin-left: 0.3em;
    width: 4em;
    height: 0.8em;
    font-size: 1.1em;
  }

  button {
    color: #0c6281df;
    margin-left: 1%;
    font-weight: 500;
  }

  button:disabled,
  button[disabled]{
    color: #8ca7b0df;
    border-color: white;
  }

  i {
    font-size: 1.2em;
  }

  .video_options{
    border: rgb(6, 25, 95) solid;
    border-radius: 1.5em;
    border-width: 0.07em;
    margin: 20px 32% 0px 32%;
    padding : 5px;
  }

  .label{
    font-size: 0.6em;
    color: brown;
  }

  .input-range{
    margin-top: 1%;
    width: 97%;
  }
</style>